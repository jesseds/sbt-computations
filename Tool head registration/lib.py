import numpy as n
from tifffile import imread, imwrite, memmap
import matplotlib.pyplot as plt
from lmfit import models, Parameters, minimize, fit_report
from dask.array import array
from scipy.interpolate import RegularGridInterpolator, interpn
from scipy.ndimage import median_filter, gaussian_filter
import dask
from numpy import ndarray
from multiprocessing import cpu_count
from skimage.transform import hough_circle, hough_circle_peaks
from typing import *


def normalize_scalars(source: ndarray, target: ndarray) -> ndarray:
    fig, ax = plt.subplots(2, 1)
    h1_y, h1_x, _ = ax[0].hist(source.ravel(), label="source", bins=256, alpha=0.5)
    h2_y, h2_x, _ = ax[0].hist(target.ravel(), label="target", bins=256, alpha=0.5)

    h1_y /= h1_y.sum() * h1_x[1] - h1_x[0]
    h2_y /= h2_y.sum() * h2_x[1] - h2_x[0]

    ax[0].legend()
    ax[0].set_title("Raw")

    def resid(pars, x, img, unc=1):
        offset = pars["offset"]
        scale = pars["scale"]
        new_x = offset + x * scale

        new_y = n.interp(h1_x[:-1], new_x, img)

        return (h1_y - new_y) / unc

    params = Parameters()
    params.add("scale", value=1)
    params.add("offset", value=0)

    result = minimize(resid, params, args=(h2_x[:-1], h2_y))

    scale = result.params['scale']
    offset = result.params["offset"]
    ax[1].set_title("With normalization")
    ax[1].stairs(h1_y, h1_x)
    ax[1].stairs(h2_y, offset + scale * h2_x)
    plt.show()

    target = offset + scale * target

    return target


def _axis_center(img: ndarray) -> ndarray:
    img_mean = n.mean(img, axis=0)
    img_filt = gaussian_filter(img_mean, img.shape[0]//4)

    posi = n.argmax(img_filt[:, 0])[0]
    posj = n.argmax(img_filt[0, :])[0]

    return n.array([posi, posj])


def preposition(source: ndarray, target: ndarray) -> ndarray:
    src_cen = _axis_center(source)
    tgt_cen = _axis_center(target)

    dr = tgt_cen - src_cen



    # def resid(params, unc=1):
    #     di = params["di"]
    #     dj = params["dj"]
    #     rot = params["rot"]
    #
    #     Ttrans = n.array([[1, 0, di],
    #                       [0, 1, dj],
    #                       [0, 0, 1]])
    #
    #     Trot = n.array([n.cos(rot), n.sin(rot), 0],
    #                    [-n.sin(rot), n.cos(rot), 0],
    #                    [0, 0, 1]])
    #
    #     Ttot = Ttrans @ Trot

