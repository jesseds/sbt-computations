import numpy as n
import numpy.ma as ma
import numpy.linalg as la
import matplotlib.pyplot as plt
from tifffile import imwrite, memmap
from scipy import interpolate, ndimage
from time import time
from multiprocessing import cpu_count
import dask.array as da
import dask_image.ndfilters as ndf
import dask
from pathlib import Path
import matplotlib as mpl
from typing import *
from dataclasses import dataclass


@dataclass
class Config:
    img_path: str
    px_cal: float  # um / px
    window_size: Tuple[float, float]  # um x um

    # Optional config
    gauss_std: float = 10  # px
    fit_npoints: int = 25  # Number of points to fit spline onto
    eval_npoints: int = 2000  # Number of points to evaluate spline
    spline_smoothness: float = 1e4
    skip: int = 1  # Skip every nth spline position (reduce computation time for development purposes)
    ncpus: int = 8 if cpu_count() > 8 else cpu_count()


def set_axes_equal(ax):
    """
    Allow setting equal aspect ratio on 3D plots
    """
    x_limits = ax.get_xlim3d()
    y_limits = ax.get_ylim3d()
    z_limits = ax.get_zlim3d()

    x_range = abs(x_limits[1] - x_limits[0])
    x_middle = n.mean(x_limits)
    y_range = abs(y_limits[1] - y_limits[0])
    y_middle = n.mean(y_limits)
    z_range = abs(z_limits[1] - z_limits[0])
    z_middle = n.mean(z_limits)

    plot_radius = 0.5*max([x_range, y_range, z_range])

    ax.set_xlim3d([x_middle - plot_radius, x_middle + plot_radius])
    ax.set_ylim3d([y_middle - plot_radius, y_middle + plot_radius])
    ax.set_zlim3d([z_middle - plot_radius, z_middle + plot_radius])


def edges2center(ary: n.ndarray) -> n.ndarray:
    """
    Convert a histogram x-values from edges to center positions
    """
    assert len(ary.shape) == 1, "Array must be 1-dimensional"
    bin_width = ary[1] - ary[0]

    return ary[:-1] + bin_width / 2


def intermodes_threshold(array: n.ndarray, plot=False) -> float:
    """
    Guess the threshold for a bimodal distribution based on the intermodes calculation.

    Process:
    1) Compute histogram on flattened array
    2) Continually compute a gaussian filter on histogram with increasing standard deviation
    3) Once only two peaks are detected, the threshold value is the mean of the two peaks
    """
    if isinstance(array, ma.masked_array):
        # numpy.histogram ignores masked values in masked arrays, fix this here
        counts, edges = n.histogram(array.compressed().ravel(), bins=128, range=(0, 255))
    else:
        counts, edges = n.histogram(array.ravel(), bins=128, range=(0, 255))

    centers = edges2center(edges)

    for i in range(max(array.shape)):
        gauss = ndimage.gaussian_filter(counts, sigma=i)
        padded = n.pad(gauss, 1, mode="reflect")

        peak_pos = []
        for idx, val in enumerate(gauss):
            if val > padded[idx] and val > padded[idx+2]:
                peak_pos.append(centers[idx])

        n_peaks = len(peak_pos)
        if n_peaks < 2:
            raise ValueError("Failed to find threshold, detected less than 2 peaks")

        if len(peak_pos) == 2:
            if plot:
                plt.plot(centers, gauss)
                plt.plot(centers, counts)
                plt.axvline(peak_pos[0])
                plt.axvline(peak_pos[1])
                plt.show()
            return n.mean(peak_pos)


def fit_spline(img, conf: Config) -> Tuple[n.ndarray, n.ndarray]:
    """
    Fit a spline to the channel.

    This is computed by:
    1) Compute a gaussian filter on the image with large standard deviation. We only want the general,
        low frequency channel morphology and not smaller features (like small shavings or roughness.
    2) Take the mean position of each slice in the gaussian-filtered image along the channel axis (0)
    3) Fit a spline to each mean position.
    4) Compute a number of points along the fit spline (which will ultimately by the positions at which
        the cross-sectional planes are placed).
    5) Compute the 3D gradient of the evaluated positions along the spline. These gradients allow us to
        align the cross-sections orthogonally to spline.
    """
    # Load or compute gaussian image
    _gauss_path = Path() / "_gauss.tiff"
    if _gauss_path.exists():
        gauss = memmap(str(_gauss_path))
        print("Reading existing gaussian image...")
    else:
        print("No gaussian image found, creating new one...")
        channel_da = da.from_array(img)
        gauss = ndf.gaussian_filter(channel_da, sigma=conf.gauss_std).compute()
        imwrite(_gauss_path, gauss)
        gauss = memmap(_gauss_path)

    # Isolate channel region from gaussian image
    print("Extracting channel...")
    mid = intermodes_threshold(gauss)

    gauss_thresh = gauss < mid

    parts, num = ndimage.label(gauss_thresh)
    channel_mask = n.zeros_like(gauss_thresh)

    # ----------------------- CHOOSE THE CORRECT LABEL HERE --------------------------
    # The label number could change from sample to sample
    channel_mask[parts == 2] = 1

    # Generate points along channel
    x = []
    y = []
    z = []

    print("Sampling channel and fitting spline...")
    for i in range(channel_mask.shape[0]):
        ys, xs = n.where(n.take(channel_mask, i, axis=0))
        if xs.size > 0 and ys.size > 0:
            z.append(i)
            x.append(xs.mean())
            y.append(ys.mean())

    x = n.array(x)
    y = n.array(y)
    z = n.array(z)

    # Resample channel points and fit spline
    tck, u = interpolate.splprep([x, y, z], k=3, s=conf.spline_smoothness)
    spl_x, spl_y, spl_z = interpolate.splev(n.linspace(0, 1, conf.eval_npoints), tck)
    # plt.plot(spl_z*conf.px_cal, spl_x*conf.px_cal)
    # plt.show()
    spl_grad = n.hstack([n.gradient(spl_x)[None].T, n.gradient(spl_y)[None].T, n.gradient(spl_z)[None].T])
    return n.array([spl_x, spl_y, spl_z]).T, spl_grad


def oblique_slices(img: n.ndarray,
                   spl_xyz: n.ndarray,
                   spl_grad: n.ndarray,
                   conf: Config) -> n.ndarray:
    """
    Extract oblique cross-sections from the channel, oriented orthogonally to the spline.

    1) Generate un-transformed coordinates representing pixels in the cross-sectional window
    2) For each slice:
        - Compute a new transformation matrix to re-center the window coordinates around the spline position and
            re-orient to be orthogonal to the spline gradient
        - Compute linear interpolation at each window coordinate with the raw 3D image
    """
    t0 = time()
    print("Computing oblique cross-sections")
    assert spl_xyz.shape == spl_grad.shape

    if conf.ncpus == 1:
        return _oblique_slices_impl(img, spl_xyz, spl_grad, conf)

    chunk_len = int(spl_xyz.shape[0] // conf.ncpus)
    tasks = []

    for i in range(conf.ncpus):
        start = i*chunk_len

        # Make sure last chunk includes all remaining elements
        if i == conf.ncpus - 1:
            end = spl_xyz.shape[0]
        else:
            end = i * chunk_len + chunk_len

        task = dask.delayed(_oblique_slices_impl)(img, spl_xyz[start:end, ...], spl_grad[start:end, ...], conf)
        tasks.append(task)

    results = dask.compute(tasks)
    retn = n.concatenate(*results)
    print(f"Completed {retn.shape[0]:,} cross-sections in {time()-t0:0,.2f} seconds")
    return retn


def _oblique_slices_impl(img: n.ndarray,
                         spl_xyz: n.ndarray,
                         spl_grad: n.ndarray,
                         conf: Config) -> n.ndarray:
    """
    Extract oblique cross-sections from the channel, oriented orthogonally to the spline.

    1) Generate un-transformed coordinates representing pixels in the cross-sectional window
    2) For each slice:
        - Compute a new transformation matrix to re-center the window coordinates around the spline position and
            re-orient to be orthogonal to the spline gradient
        - Compute linear interpolation at each window coordinate with the raw 3D image
    """
    spl_x = spl_xyz[:, 0]
    spl_y = spl_xyz[:, 1]
    spl_z = spl_xyz[:, 2]

    window_size = conf.window_size
    px_cal = conf.px_cal
    skip = conf.skip

    win_x = n.linspace(-window_size[0]/2, window_size[0]/2, int(window_size[0]//px_cal))
    win_y = n.linspace(-window_size[1]/2, window_size[1]/2, int(window_size[1]//px_cal))
    Z, Y, X = n.meshgrid(0, win_y, win_x, indexing="ij")

    all_xyz = n.hstack([
         X.ravel()[None].T,
         Y.ravel()[None].T,
         Z.ravel()[None].T,
         n.ones([Z.size, 1])
         ])

    retn = n.zeros([spl_x.shape[0]//skip, win_y.size, win_x.size])

    for count, idx in enumerate(range(0, spl_grad.shape[0], skip)):
        grad_x = spl_grad[idx, 0]
        grad_y = spl_grad[idx, 1]
        grad_z = spl_grad[idx, 2]

        theta_x = n.arctan(grad_y/grad_z)
        theta_y = n.arctan(grad_z/grad_x) + n.pi/2

        if theta_y > n.pi/2:
            theta_y -= n.pi

        rot_x = n.array([
            [1, 0,               0,              0],
            [0, n.cos(theta_x),  n.sin(theta_x), 0],
            [0, -n.sin(theta_x), n.cos(theta_x), 0],
            [0, 0,               0,              1]
        ])

        rot_y = n.array([
            [n.cos(theta_y), 0, -n.sin(theta_y), 0],
            [0,              1, 0,               0],
            [n.sin(theta_y), 0, n.cos(theta_y),  0],
            [0,              0, 0,               1]
        ])

        shift = n.array([
            [1, 0, 0, spl_x[idx]*px_cal],
            [0, 1, 0, spl_y[idx]*px_cal],
            [0, 0, 1, spl_z[idx]*px_cal],
            [0, 0, 0, 1]
        ])

        T = shift @ rot_y @ rot_x
        win_trans_zyx = n.flip((T @ all_xyz.T).T[:, :3], 1)

        img_x = n.arange(img.shape[2]) * px_cal
        img_y = n.arange(img.shape[1]) * px_cal
        img_z = n.arange(img.shape[0]) * px_cal

        img_interpolator = interpolate.RegularGridInterpolator(
            (img_z, img_y, img_x),
            img,
            method="linear",
            fill_value=0,
            bounds_error=False
        )

        pts_interp = img_interpolator(win_trans_zyx).reshape([retn.shape[1], retn.shape[2]])
        retn[count, :, :] = pts_interp

    return retn


def cross_section_metrics(img: n.ndarray, conf: Config, filter_val=0):
    """
    Compute per-cross-section metrics.

    Notes:
    - The first axis (0) should be the channel direction.
    - filter_val indicates values to be ignored (i.e. outside of volume)
    """

    filt = img == filter_val
    data = ma.masked_array(img, filt)
    px_cal = conf.px_cal

    thresh = intermodes_threshold(data)

    areas = n.zeros(data.shape[0])
    for i in range(data.shape[0]):
        raw = data[i, ...]
        channel_vals = raw <= thresh
        area = n.count_nonzero(channel_vals.compressed()) * px_cal**2
        areas[i] = area

    return areas
