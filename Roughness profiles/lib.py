import tifffile as tif
import matplotlib.pyplot as plt
import numpy as n
import numpy.ma as ma
from scipy import ndimage
import os
from scipy.interpolate import RegularGridInterpolator
import csv
import shutil
from typing import *
from dataclasses import dataclass
from pathlib import Path

@dataclass
class SBTFile:
    path: str
    dat: n.ndarray
    cal: float
    thresh: float
    ax0: n.ndarray
    ax1: n.ndarray
    ax2: n.ndarray
    cen_abs: n.ndarray
    nprofs: int = 9
    dist: float = 0.25


def edges2center(ary: n.ndarray) -> n.ndarray:
    """
    Convert a histogram x-values from edges to center positions
    """
    assert len(ary.shape) == 1, "Array must be 1-dimensional"
    bin_width = ary[1] - ary[0]

    return ary[:-1] + bin_width / 2


def intermodes_threshold(array: n.ndarray, plot=False) -> float:
    """
    Guess the threshold for a bimodal distribution based on the intermodes calculation.

    Process:
    1) Compute histogram on flattened array
    2) Continually compute a gaussian filter on histogram with increasing standard deviation
    3) Once only two peaks are detected, the threshold value is the mean of the two peaks
    """
    if isinstance(array, ma.masked_array):
        # numpy.histogram ignores masked values in masked arrays, fix this here
        counts, edges = n.histogram(array.compressed().ravel(), bins=128, range=(0, 255))
    else:
        counts, edges = n.histogram(array.ravel(), bins=128, range=(array.min(), array.max()))

    centers = edges2center(edges)

    for i in range(max(array.shape)):
        gauss = ndimage.gaussian_filter(counts, sigma=i)
        padded = n.pad(gauss, 1, mode="reflect")

        peak_pos = []
        for idx, val in enumerate(gauss):
            if val > padded[idx] and val > padded[idx+2]:
                peak_pos.append(centers[idx])

        n_peaks = len(peak_pos)
        if n_peaks < 2:
            raise ValueError("Failed to find threshold, detected less than 2 peaks")

        if len(peak_pos) == 2:
            if plot:
                plt.plot(centers, gauss)
                plt.plot(centers, counts)
                plt.axvline(peak_pos[0])
                plt.axvline(peak_pos[1])
                plt.show()
            return n.mean(peak_pos)


def prepare(filepath: str) -> SBTFile:
    tiff_file = tif.TiffFile(filepath)
    if "spacing" not in tiff_file.imagej_metadata.keys():
        raise RuntimeError("Tiff file does not have spatial calibration embedded")
    cal = tiff_file.imagej_metadata["spacing"] * 10  # mm/px
    # cal = scale

    dat = tif.imread(filepath)
    ax0 = n.linspace(0, dat.shape[0] * cal, dat.shape[0])
    ax1 = n.linspace(0, dat.shape[1] * cal, dat.shape[1])
    ax2 = n.linspace(0, dat.shape[2] * cal, dat.shape[2])

    cen_px = n.array(dat.shape) // 2
    cen_abs = (n.array(dat.shape) * cal) / 2
    thresh = intermodes_threshold(dat)

    return SBTFile(filepath, dat, cal, thresh, ax0, ax1, ax2, cen_abs)


def get_profiles(sbt: SBTFile, n_profiles=1, dist=1) -> List[Tuple[n.ndarray, n.ndarray]]:
    assert n_profiles % 2 == 1
    assert n_profiles >= 1

    ax0 = sbt.ax0
    ax1 = sbt.ax1
    ax2 = sbt.ax2
    cen_abs = sbt.cen_abs
    cal = sbt.cal

    interp = RegularGridInterpolator((ax0, ax1, ax2), sbt.dat)

    lower = cen_abs[2] - n_profiles * dist / 2
    upper = cen_abs[2] + n_profiles * dist / 2
    span = n.linspace(lower, upper, n_profiles)

    retn = []

    for i in n.linspace(lower, upper, n_profiles):
        I, J = n.meshgrid(ax0, ax1, indexing="ij")
        pts = n.hstack((I.ravel()[None].T, J.ravel()[None].T, n.ones([I.size, 1]) * i))

        try:
            pln = interp(pts)
        except Exception as exc:
            raise RuntimeError("Attempting interpolate outside of image boundaries, check input parameters or image orientation")
        pln = pln.reshape(I.shape).T

        contours = plt.contour(pln, levels=[sbt.thresh], colors=["white"])

        paths = contours.collections[0].get_paths()

        paths = sorted(paths, key=lambda x: x.vertices.shape[0], reverse=True)

        path0 = paths[0].vertices * cal
        path1 = paths[1].vertices * cal

        retn.append(sorted((path0, path1), key=lambda x: x[:, 1].mean()))

    return retn


def write_output(sbt: SBTFile, results):
    # Process output files
    prof_mid = (sbt.nprofs - 1) // 2
    for idx, (sbt, profs) in enumerate(results):

        fig, ax = plt.subplots(1, 1, figsize=(20, 6))

        cmap = plt.cm.RdYlGn
        cols = cmap(n.linspace(0, 1, sbt.nprofs))
        sample = sbt.path.name.split(".")[0]
        ax.set_xlabel("Channel x position (mm)")
        ax.set_ylabel("Channel y position (mm)")
        ax.set_title(sample)

        for idx, (i, j) in enumerate(profs[::-1]):
            ax.plot(i[:, 0], i[:, 1], color=cols[idx], alpha=0.5)
            ax.plot(j[:, 0], j[:, 1], color=cols[idx], alpha=0.5)

        try:
            shutil.rmtree(sample)
        except:
            pass

        os.mkdir(sample)
        os.mkdir(f"{sample}/lower")
        os.mkdir(f"{sample}/upper")

        fig.savefig(f"{sample}/{sample}.png")
        plt.close("all")

        for idx, i in enumerate(profs):
            dy = - (prof_mid - idx) * sbt.dist

            upper = i[0]
            lower = i[1]

            # upper = pd.DataFrame()
            # lower = pd.DataFrame()
            #
            # upper["x (mm)"] = i[0][:, 0]
            # lower["x (mm)"] = i[1][:, 0]
            #
            # upper[f"y (mm)"] = i[0][:, 1]
            # lower[f"y (mm)"] = i[1][:, 1]

            # upper.to_csv(f"{sample}/upper/{dy}.csv", sep=",", index=False)
            # lower.to_csv(f"{sample}/lower/{dy}.csv", sep=",", index=False)

            _write_csv(f"{sample}/upper/{dy}.csv", upper)
            _write_csv(f"{sample}/lower/{dy}.csv", lower)


def _write_csv(path, ary: n.ndarray):
    with open(path, "w", newline="") as file:
        csv_file = csv.writer(file)
        csv_file.writerow(["x (mm)", "y (mm)"])
        for i, j in ary:
            csv_file.writerow([i, j])

