import lib
from tqdm import tqdm
import os
from pathlib import Path
from warnings import warn
import sys
import argparse
import traceback
import time


def exit(delay=5):
    print(f"Exiting in {delay} sec...")
    time.sleep(delay)
    sys.exit()


if __name__ == "__main__":
    descr = """
    This program generates 'roughness' profiles for SBT XRM samples.
    
    NOTE 1: The input images are assumed to be oriented such that the first dimension is down the channel.
    
    Place this program in a folder containing 3D tifs of channels (all tif files
    directly in the folder will be processed). A new folder will be created for each
    tif containing a plot image of the profiles and 2 folders containing the upper/lower
    profiles data. The center position is taken as the geometric center of the tif image.
    
    The number of profiles and separation between them can be configured as below.
    """
    parse = argparse.ArgumentParser(description=descr,
                                    # formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                    formatter_class=argparse.RawDescriptionHelpFormatter
                                    )

    parse.add_argument("-n", "--nprofs", help="Number of profiles to generate", default=21)
    parse.add_argument("-d", "--dist", help="Distance between slices", default=0.25)
    args = parse.parse_args()
    conf = vars(args)

    nprofs = conf["nprofs"]
    dist = conf["dist"]

    try:
        nprofs = int(nprofs)
    except:
        print("Could not interpret nprofs input as integer")
        exit()

    try:
        dist = float(dist)
    except:
        print("Could not interpret dist input as number")
        exit()

    if nprofs % 2 != 1 or nprofs < 1:
        print("nprofs must be odd and non-zero")
        exit()

    if dist <= 0:
        print("dist must be positive and non-zero")
        exit()

    print(f"{conf['nprofs']} profiles will be generated per file separated by {conf['dist']} mm")
    print(f"This configuration spans a {nprofs*dist} mm width")

    tifs = []

    # root_dir = os.path.abspath(os.path.dirname(__file__))

    for root, dirs, files in os.walk(os.getcwd()):
        for file in files:
            path = Path(file)

            if path.suffix in (".tif", ".tiff"):
                tifs.append(path)

        # Only search root directory
        break

    if len(tifs) == 0:
        print("No tif files found in current directory")
        exit()
    else:
        print("These tif files will be processed:")
        for i in tifs:
            print(f"\t{i.name}")
        result = input("Continue? (y/n): ")
        if result != "y":
            sys.exit()

    prof_mid = (nprofs - 1) // 2

    results = []

    failures = []
    for i, path in enumerate(tqdm(tifs, ncols=100)):
        try:
            sbt_file = lib.prepare(path)
            sbt_file.nprofs = nprofs
            sbt_file.dist = dist

            profs = lib.get_profiles(sbt_file, nprofs, dist)
            results.append((sbt_file, profs))
            lib.write_output(sbt_file, results)
        except Exception as exc:
            failures.append((path, exc))

    time.sleep(0.1)
    print(f"Successfully processed {len(tifs)-len(failures)}/{len(tifs)} tif files")

    if len(failures) > 0:
        print("Failed to process these files:")

        for path, exc in failures:
            print(f"\"{path.name}\" due to error:")
            print(f"\t{exc.args[0]}")

    exit()

