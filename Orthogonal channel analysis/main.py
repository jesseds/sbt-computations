from lib import *
from lmfit import models


if __name__ == "__main__":
    conf = Config(
        img_path=r"/run/media/zoot/T7/2022-09-29 Sine channel stitch/recon/cropped_bin2_32.tif",
        px_cal=14.6786e-3,  # mm / px
        window_size=(6000e-3, 2500e-3),  # mm x mm
        eval_npoints=2000,
        gauss_std=10,
        fit_npoints=100,
        spline_smoothness=1e4
    )

    # --------- Calculation ---------
    # Load image as a memory-map
    img = memmap(conf.img_path)
    img_x = n.arange(img.shape[2]) * conf.px_cal
    img_y = n.arange(img.shape[1]) * conf.px_cal
    img_z = n.arange(img.shape[0]) * conf.px_cal

    # Fit spline to channel
    spl_ijk, spl_grad = fit_spline(img, conf)
    spl_xyz = spl_ijk * conf.px_cal  # Cartesian coordinates not px

    # Extract oblique slices
    slices = oblique_slices(img, spl_ijk, spl_grad, conf)

    # Save output
    imwrite("obliques.tiff", slices)

    # Compute metrics
    areas = cross_section_metrics(slices, conf)
    n.savetxt("results.csv", areas)

    # Prepare some data and the sine fit for plotting
    diff = n.diff(spl_xyz, axis=0)
    sep = la.norm(diff, axis=1)
    sep = n.concatenate([n.array([0]), sep.ravel()])
    sep = n.cumsum(sep)
    fit_rng = (4.5, 33.8)
    fit_idx = (
        n.argmin(n.abs(sep - fit_rng[0])),
        n.argmin(n.abs(sep - fit_rng[1])),
    )
    spl_x_fit = spl_xyz[fit_idx[0]:fit_idx[1], 0]
    spl_y_fit = spl_xyz[fit_idx[0]:fit_idx[1], 1]
    spl_z_fit = spl_xyz[fit_idx[0]:fit_idx[1], 2]
    sep_fit = sep[fit_idx[0]:fit_idx[1]]

    # Plot the spline overlaid on the projections
    ext1 = (img_x.min(), img_x.max(), img_z.max(), img_z.min())
    ext2 = (img_z.min(), img_z.max(), img_y.max(), img_y.min())
    fig_proj, ax_proj = plt.subplots(1, 2)
    ax_proj[0].imshow(img.mean(axis=1), extent=ext1)
    ax_proj[0].plot(spl_x_fit, spl_z_fit, "--", c="white")
    ax_proj[1].imshow(img.mean(axis=2).T, extent=ext2)
    ax_proj[1].plot(spl_z_fit, spl_y_fit, "--", c="white")
    ax_proj[0].set_xlabel("X position (mm)")
    ax_proj[0].set_ylabel("Z position (mm)")
    ax_proj[1].set_xlabel("Z position (mm)")
    ax_proj[1].set_ylabel("Y position (mm)")
    plt.show()

    # Plot the cross-section metrics and spline data

    width = areas.shape[0]//15
    moving_avg = n.convolve(areas, n.ones(width), mode="same") / width
    x = sep

    # Fit sine
    sine = models.SineModel()
    sine_init = sine.guess(spl_x_fit, x=sep_fit)
    corr = models.LinearModel()
    model = sine + corr
    params = model.make_params()
    params.update(sine_init)
    fit = model.fit(spl_x_fit, x=sep_fit, params=params)

    def x2deg(x):
        return n.rad2deg(fit.params["frequency"].value * x + fit.params["shift"].value - 3*n.pi/2)

    def deg2x(deg):
        return (n.deg2rad(deg) - fit.params["shift"].value + 3*n.pi/2)/fit.params["frequency"].value


    fig, ax1 = plt.subplots()

    ax2 = ax1.twinx()
    ax_deg = ax1.secondary_xaxis("top", functions=(x2deg, deg2x))
    ax_deg.set_xlabel("Sine shift [deg]")
    ax1.axvline(deg2x(0), c="black")

    ax1.plot(x, areas, ".", c="0.5", label="Cross-sectional area")
    ax1.plot(x[width//2:-width//2], moving_avg[width//2:-width//2], c="0", label=f"Moving average")
    ax2.plot(x, spl_xyz[:, 0], "--", label="Spline x")
    ax2.plot(x, spl_xyz[:, 1], "--", label="Spline y")
    ax2.plot(x, spl_xyz[:, 2], "--", label="Spline z")
    fit_lbl = "$f(x|...) = A\cos{(kx+\phi)} + Bx + C$"

    ax2.plot(sep_fit, fit.best_fit, label=fit_lbl)

    ax1.legend()
    ax2.legend()
    ax1.set_xlabel(f"Position (spline coordinates) [mm]")
    ax1.set_ylabel(f"Area [mm2]")
    ax2.set_ylabel(f"Spline position (cartesian) [mm]")
    plt.show()
